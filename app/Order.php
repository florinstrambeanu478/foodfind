<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    protected $fillable = ['total'];

    public function users()
    {
        return $this->belongsToMany(User::class, 'order_user')->withTimestamps();
    }

    public function menus()
    {
        return $this->belongsToMany(Menu::class, 'order_menu')->withTimestamps();
    }

}
