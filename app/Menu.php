<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $fillable=['name', 'description', 'price'];

    public function users()
    {
       return $this->belongsToMany(User::class, 'menu_user')->withTimestamps();
    }

    public function orders()
    {
        return $this->belongsToMany(Order::class, 'order_menu')->withTimestamps();
    }

}
