<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Foood</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: white;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                margin: 0;
            }

            * {
                box-sizing: border-box;
            }

            .full-height {
                height: 100vh;
            }

            .menu_title {
                display: grid;
                margin-top: 0;
                margin-left:570px;
                margin-right:1000px;
                font-size:80px;
                font-family: cursive;


            }

            .container {
                display: grid;
                grid-template-columns: auto auto auto;
                grid-template-rows: auto auto auto;
                width:auto;
                margin-left:auto;
                margin-right:auto;
                height:auto;
                padding: 30px;
                grid-gap: 2em;
                background-color: white;


            }

            .list {
                display:grid;
                grid-template-columns: 500px;
                grid-template-rows: repeat(20, 40px);
                width:auto;
                height:auto;
                margin-left: 50px;
                margin-right:200px;
                background-repeat: no-repeat;
                background-position: center;


            }
            .list {
                background-image: url('https://us.123rf.com/450wm/akz/akz1401/akz140100684/25090211-old-vintage-yellow-page-paper-texture-or-background.jpg?ver=6');
                background-size: cover ;
            }

            .title {
                font-size: 40px;
                text-align: center;
            }

            .shopping-item{
                border-bottom: 1px solid black;

            }

            .item {
                width:300px;
                height: 300px;
            }

            .button {
                transform: scale(1);
                transition-duration: 300ms;
            }

            .button:hover {
                cursor: pointer;
                transform: scale(1.1);
                box-shadow:0 10px 20px 0 darkgray;
                transition-duration: 300ms;
            }

            .image > img{
                object-fit: cover;
            }


            .text {
                font-family: cursive;
                font-size: 15px;
            }

            .description {

            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .d-flex {
                display: flex;
                flex-direction: row;
                justify-content: space-evenly;
            }

            .l-flex {
                display: flex;
                flex-direction: row;
                justify-content: center;
            }

        </style>
    </head>
    <body>

    <div class="top-right">
        @if (Route::has('login'))
            <div class="top-right links">
                @auth
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                @endauth
            </div>
        @endif
    </div>

    <div class="text">
        hello {{ Auth::user()->name }}, what's on the menu today?
    </div>

    <div class="menu_title">
        <h3>Menus</h3>
    </div>
    <div class="l-flex">
    <div class="container">
        <div class="item button">
            <div class="menu ">
                <div class="image">
                    <img src="images/2 sandwich.jpg" alt="samiches" style="width:300px;height:250px">
                </div>
                <div class="description">
                    <div class="text d-flex">
                    Two Delicious Sandwiches with ingredients
                    </div>
                    <div class="text d-flex">
                        <div class="flex-item">
                            33,11 LEI
                        </div>
                        <div class="flex-item">
                            From Gucci
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="item button">
            <div class="menu ">
                <div class="image">
                <img src="images/korean.jpg" alt="korean" style="width:300px;height:250px">
                </div>
                    <div class="description">
                    <div class="text d-flex">
                        Korean sake
                    </div>
                    <div class="text d-flex">
                        <div class="flex-item">
                            45,78 LEI
                        </div>
                        <div class="flex-item">
                            From Kim
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="item button">
            <div class="menu">
                <div class="image">
                <img src="images/mexican food.jpg" alt="mexican" style="width:300px;height:250px">
                </div>
                <div class="description">
                    <div class="text d-flex">
                        Tortilla con sos
                    </div>
                    <div class="text d-flex">
                        <div class="flex-item">
                            39,00 LEI
                        </div>
                        <div class="flex-item">
                            From Juan
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="item button">
            <div class="menu">
                <div class="image">
                <img src="images/pancakes.jpg" alt="pancakes" style="width:300px;height:250px">
                </div>
                <div class="description">
                    <div class="text d-flex">
                        Pancakes with syrup
                    </div>
                    <div class="text d-flex">
                        <div class="flex-item">
                            18,00 LEI
                        </div>
                        <div class="flex-item">
                            From Buldoghiak
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="item button">
            <div class="menu">
                <div class="image">
                <img src="images/pizza.jpg" alt="pizza" style="width:300px;height:250px">
                </div>
                <div class="description">
                    <div class="text d-flex">
                        Pizza with ceapa
                    </div>
                    <div class="text d-flex">
                        <div class="flex-item">
                            44,00 LEI
                        </div>
                        <div class="flex-item">
                            From San Torino
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="item button">
            <div class="menu">
                <div class="image">
                <img src="images/salad.jpg" alt="salad" style="width:300px;height:250px">
                </div>
                <div class="description">
                    <div class="text d-flex">
                        Salad con pasta y Pesto
                    </div>
                    <div class="text d-flex">
                        <div class="flex-item">
                            25,76 LEI
                        </div>
                        <div class="flex-item">
                            From Malbonna
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="item button">
            <div class="menu">
                <div class="image">
                <img src="images/spaghetti.jpg" alt="spaghetti" style="width:300px;height:250px">
                </div>
                <div class="description">
                    <div class="text d-flex">
                        Spaghetti
                    </div>
                    <div class="text d-flex">
                        <div class="flex-item">
                            23,53 LEI
                        </div>
                        <div class="flex-item">
                            From Chialmanduro
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="item button">
            <div class="menu">
                <div class="image">
                <img src="images/thic pizza.jpg" alt="thic pizza" style="width:300px;height:250px">
                </div>
                <div class="description">
                    <div class="text d-flex">
                        Peperoni Pizza
                    </div>
                    <div class="text d-flex">
                        <div class="flex-item">
                            42,00 LEI
                        </div>
                        <div class="flex-item">
                            From Lombardia
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="item button">
            <div class="menu">
                <div class="image">
                <img src="images/wrap.jpg" alt="wrap" style="width:300px;height:250px">
                </div>
                <div class="description">
                    <div class="text d-flex">
                        Wrap
                    </div>
                    <div class="text d-flex">
                        <div class="flex-item">
                            21,42 LEI
                        </div>
                        <div class="flex-item">
                            From Fagholianto
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <div class="list">
            <div class="title">
                Shopping List
            </div>
            <div class="shopping-item">

            </div>
            <div class="shopping-item">

            </div>
            <div class="shopping-item">

            </div>
            <div class="shopping-item">

            </div>
            <div class="shopping-item">

            </div>
            <div class="shopping-item">

            </div>
            <div class="shopping-item">

            </div>
            <div class="shopping-item">

            </div>
            <div class="shopping-item">

            </div>
            <div class="shopping-item">

            </div>
            <div class="shopping-item">

            </div>
            <div class="shopping-item">

            </div>
            <div class="shopping-item">

            </div>
            <div class="shopping-item">

            </div>
            <div class="shopping-item">

            </div>
            <div class="shopping-item">

            </div>
            <div class="shopping-item">

            </div>
            <div class="shopping-item">

            </div>
            <div class="shopping-item">

            </div>

        </div>
    </div>





    </body>
</html>
